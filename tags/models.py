from django.db import models


class Tag(models.Model):
    name = models.CharField(max_length=20)
    recipes = models.ManyToManyField("recipes.Recipe", related_name=("tags"))

    def __string__(self):
        return self.name
